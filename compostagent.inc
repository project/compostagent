<?php
/**
 * @file
 * General functions and definitions for the Command Post agent module.
 */


/**
 * Return the pair timeout.
 */
function compostagent_get_pair_timeout() {
  return variable_get('compostagent_pair_timeout', 60);
}


/**
 * Check if the current connection is secure.
 *
 * @return bool
 *   TRUE if the connection is secure, FALSE if not.
 */
function _compostagent_is_secure() {
  $https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : 'n';
  return ($https[0] == 'y' || $https[0] == 'Y' || $https[0] == '1');
}


/**
 * Transform a string terminated in k/m/g into bytes.
 *
 * Transform "1k" => 1024, "1m" => 1024*1024, and so on.
 *
 * @param string $str
 *   The input string.
 *
 * @return int
 *   The equivalent of $str in bytes.
 */
function _compostagent_strbytes($str) {
  if (!$str) {
    return 0;
  }

  $last = drupal_substr($str, -1);
  switch ($last) {
    case 'G':
    case 'g':
      $str *= 1024 * 1024 * 1024;
      break;

    case 'M':
    case 'm':
      $str *= 1024 * 1024;
      break;

    case 'K':
    case 'k':
      $str *= 1024;
  }

  return $str;
}


/**
 * Compares two string in constant time.
 *
 * Under PHP >= 5.6, this function simply calls hash_equals().
 */
function compostagent_hash_equals($known_string, $user_string) {
  if (function_exists('hash_equals')) {
    return hash_equals($known_string, $user_string);
  }

  $len_known = strlen($known_string);
  $len_user = strlen($user_string);

  if ($len_known != $len_user) {
    return FALSE;
  }

  $nulls = $known_string ^ $user_string;
  $result = 0;
  for ($i = 0; $i < $len_known; $i++) {
    $result |= ord($nulls[$i]);
  }

  return !$result;
}


/**
 * Returns current pairing information.
 *
 * @return array
 *   Pairing information array. NULL if no information is available,
 *   or if the previous information was expired.
 */
function compostagent_get_pair_info() {
  $info = variable_get('compostagent_pair_info', NULL);

  if ($info && $info['timestamp'] < (REQUEST_TIME - compostagent_get_pair_timeout())) {
    $info = NULL;
  }

  return $info;
}


/**
 * Derives a key from a password using PBKDF2.
 *
 * Based on
 * https://github.com/defuse/password-hashing/blob/master/PasswordHash.php
 * PBKDF2 implementation.
 *
 * @see http://php.net/manual/en/function.hash-pbkdf2.php
 */
function compostagent_pbkdf2($algorithm, $password, $salt, $count,
                             $key_length = 0, $raw_output = FALSE) {
  $algorithm = strtolower($algorithm);
  if (!in_array($algorithm, hash_algos(), TRUE)) {
    throw new RuntimeException('PBKDF2 ERROR: Invalid hash algorithm:' . $algorithm);
  }
  if ($count <= 0 || $key_length <= 0) {
    throw new RuntimeException('PBKDF2 ERROR: Invalid parameters.');
  }
  if (function_exists("hash_pbkdf2")) {
    // The output length is in NIBBLES (4-bits) if $raw_output is false!
    if (!$raw_output) {
      $key_length = $key_length * 2;
    }
    return hash_pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output);
  }
  $hash_length = strlen(hash($algorithm, "", TRUE));
  $block_count = ceil($key_length / $hash_length);
  $output = "";
  for ($i = 1; $i <= $block_count; $i++) {
    // $i encoded as 4 bytes, big endian.
    $last = $salt . pack("N", $i);
    // First iteration.
    $last = $xorsum = hash_hmac($algorithm, $last, $password, TRUE);
    // Perform the other $count - 1 iterations.
    for ($j = 1; $j < $count; $j++) {
      $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, TRUE));
    }
    $output .= $xorsum;
  }
  if ($raw_output) {
    return substr($output, 0, $key_length);
  }
  else {
    return bin2hex(substr($output, 0, $key_length));
  }
}

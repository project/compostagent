Introduction
============
This the Command Post agent module. It should be installed on Drupal
sites that are going to be managed by Command Post.



Installation
============
Install as you would normally install a contributed Drupal module. See
https://drupal.org/documentation/install/modules-themes/modules-7 for
further information.



Configuration
=============
To configure the Command Post agent:

1. Go to "Configuration > System > Command Post agent"
(admin/config/system/compostagent)

2. Take note of your *Agent key* that was generated during
installation.

   IMPORTANT: You should give this key only the Command Post
   administrator that will to monitor and control your site (usually
   the same person that asked you to install this module). Be extra
   careful when dealing with keys managed by Command Post, as they
   will allow anyone who possess them to monitor and control your
   sites. You definitely do NOT want to communicate keys using e-mail
   or other insecure media.

3. Input the Command Post key. You should have received this value
   from your the Command Post administrator.



Pairing
=======
You can also configure the module by pairing it with a Command Post
site. To do that, follow the steps below:

1. Establish some form of synchronous communication with the Command
   Post administrator so that you can exchange a pairing PIN. For
   example, you may call the admin by phone, or use secure instant
   messaging.

   Notice: certify that you are talking with right person before
   pairing your site! Anyone that you pair your site with will be able
   to monitor your site, and even run commands on it!

2. Go to the agent configuration page
   (admin/config/system/compostagent) and click "Pair this site". This
   will display your pairing PIN.

3. Inform the PIN to the administrator. If everything went well, s/he
   will inform you that the your site was paired successfully.

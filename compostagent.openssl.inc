<?php
/**
 * @file
 * Classes for dealing with encryption using OpenSSL.
 */


/**
 * An AES cipher using OpenSSL.
 */
class CompostAgentOpenSSLAESCipher extends CompostAgentCipher {
  protected $bits;
  protected $mode;

  /**
   * Constructs the object.
   *
   * @param int $bits
   *   AES key size (currently only 128 is supported)
   *
   * @param string $mode
   *   AES cipher mode (currently only "cbc" is supported")
   */
  public function __construct($bits, $mode) {
    assert('$bits == 128');
    assert('$mode == "cbc"');
    $this->bits = $bits;
    $this->mode = $mode;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'aes-' . $this->bits . '-' . $this->mode;
  }

  /**
   * Derives a key from a password.
   *
   * This is used to derive a key from an arbitrary length password.
   */
  protected function deriveKey($password) {
    return substr(hash('sha1', $password, TRUE), 0, 16);
  }

  /**
   * Encrypts data with AES using a key derived from a password.
   *
   * @param string $data
   *   The data to be encrypted.
   *
   * @param string $password
   *   The password to use for decryption.
   *
   * @see CompostAgentOpenSSLAESCipher::deriveKey()
   */
  public function encrypt($data, $password) {
    $key = $this->deriveKey($password);

    $cipher = $this->getName();

    $iv_size = openssl_cipher_iv_length($cipher);
    $iv = openssl_random_pseudo_bytes($iv_size);

    return $iv . openssl_encrypt($data, $cipher, $key, OPENSSL_RAW_DATA, $iv);
  }

  /**
   * Decrypts data with AES using a key derived from a password.
   *
   * @param string $data
   *   The data to be decrypted.
   *
   * @param string $password
   *   The password to use for decryption.
   *
   * @see CompostAgentOpenSSLAESCipher::deriveKey()
   */
  public function decrypt($data, $password) {
    $key = $this->deriveKey($password);

    $cipher = $this->getName();
    $iv_size = openssl_cipher_iv_length($cipher);

    $iv = substr($data, 0, $iv_size);
    $data = substr($data, $iv_size);

    return openssl_decrypt($data, $cipher, $key, OPENSSL_RAW_DATA, $iv);
  }
}


/**
 * The OpenSSL object factory.
 */
class CompostAgentOpenSSLCipherFactory extends CompostAgentCipherFactory {

  /**
   * Constructs a new OpenSSL cipher object for the supplied algorithm.
   *
   * @param string $algo
   *   The cipher algorithm (e.g. 'aes-128-cbc')
   */
  public function newCipher($algo) {
    if ($algo == 'aes-128-cbc') {
      return new CompostAgentOpenSSLAESCipher(128, 'cbc');
    }

    throw new CompostAgentException("Unsupported openssl cipher algorithm: $algo");
  }
}

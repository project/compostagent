(function ($) {

  function hide_key() {
    var $this = $(this);

    if ($this.val() != '') {
      var $fake_key = $('<input type="password">')
          .attr('disabled', 'disabled')
          .attr('size', $this.attr('size'))
          .val('?'.repeat(32));
      var $show_link = $('<a>')
          .attr('href', '#')
          .text(Drupal.t('Edit'))
          .click(function() {
            // Hide link.
            $(this).hide();
            // Fade input field in.
            $fake_key.fadeOut(function () {
              $this.fadeIn();
            });
            return false;
          });
      $this
        .after($show_link)
        .after($fake_key);
    }
    else {
      $this.show();
    }
  }

  function attach_behavior(context, settings) {
    $("input#edit-compostagent-key,input#edit-compostagent-manager-key", context)
      .once('compost-key')
      .each(hide_key);
  }

  Drupal.behaviors.compostModule = {
    attach: attach_behavior,
  };

})(jQuery);

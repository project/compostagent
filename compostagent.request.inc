<?php
/**
 * @file
 * Handles requests from the OC master.
 */

require_once 'compostagent.compost.inc';
require_once 'compostagent.inc';


/**
 * Abstract class for cipher objects.
 */
abstract class CompostAgentCipher {

  /**
   * Returns the algorithm name.
   */
  abstract public function getName();

  /**
   * Encrypts data using a key derived from a password.
   *
   * It is up to the algorithm to decide if the password argument is
   * to be treated as straight key material and, if not, how to derive
   * a key from it.
   *
   * @param string $data
   *   The data to be encrypted.
   *
   * @param string $password
   *   The password to use for decryption.
   */
  abstract public function encrypt($data, $password);

  /**
   * Decrypts data using a key derived from a password.
   *
   * It is up to the algorithm to decide if the password argument is
   * to be treated as straight key material and, if not, how to derive
   * a key from it.
   *
   * @param string $data
   *   The data to be decrypted.
   *
   * @param string $password
   *   The password to use for decryption.
   */
  abstract public function decrypt($data, $password);
}


/**
 * Abstract factory class for cipher objects.
 */
abstract class CompostAgentCipherFactory {

  /**
   * Creates a new cipher algorithm object.
   */
  abstract public function newCipher($algo);
}


/**
 * The custom exception thrown on agent errors.
 */
class CompostAgentException extends Exception {}


/**
 * A request received by this agent.
 */
class CompostAgentRequest {
  protected $payload;
  protected $mac;
  protected $cipherAlgo;
  protected $key;

  /**
   * Constructs the request object.
   *
   * @param array $request
   *   The request array (e.g. $_POST).
   *
   * @param string $key
   *   The sender key (e.g. the manager key).
   */
  public function __construct($request, $key) {
    // Do basic sanity checks.
    if (!isset($request['msg'], $request['macalgo'], $request['mac'])) {
      throw new CompostAgentException('Incomplete request method');
    }

    if ($request['macalgo'] != 'hmac-sha256') {
      throw new CompostAgentException('Unsuported MAC algorithm: '
                                   . $request['macalgo']);
    }

    $this->payload = $request['msg'];
    $this->mac = base64_decode($request['mac']);
    if (!empty($request['cipher'])) {
      $this->cipherAlgo = $request['cipher'];
    }

    $this->key = $key;
  }

  /**
   * Determine if the request is authentic.
   *
   * The function checks if the received MAC matches the manager key
   * we have.
   */
  public function isAuthentic() {
    return compostagent_hash_equals(hash_hmac('sha256', $this->payload,
                                              $this->key, TRUE),
                                    $this->mac);
  }

  /**
   * Returns the request payload.
   */
  public function getPayload() {
    return $this->payload;
  }

  /**
   * Returns the name of the algorithm used to encrypt the payload.
   *
   * Returns NULL if the payload was not encrypted.
   */
  public function getCipherAlgo() {
    return $this->cipherAlgo;
  }
}


/**
 * A Command Post message.
 */
class CompostAgentMessage {
  protected $data = array();

  /**
   * Constructs the object.
   */
  public function __construct() {
    $this->data['time'] = REQUEST_TIME;
  }

  /**
   * Get the timestamp of the message.
   */
  public function getTime() {
    return $this->data['time'];
  }

  /**
   * Add a response to the message.
   */
  public function addResponse($request, $args = NULL) {
    if (!isset($this->data['response'])) {
      $this->data['response'] = array();
    }
    $this->data['response'][$request] = $args;
  }

  /**
   * Get an array with all 'request => args' contained in the message.
   */
  public function getRequests() {
    return $this->data['request'];
  }

  /**
   * Get a single request.
   */
  public function getRequest($request) {
    return isset($this->data['request'][$request]) ? $this->data['request'][$request] : NULL;
  }

  /**
   * Set user messages in this message object.
   */
  public function setMessages(array $messages) {
    $this->data['messages'] = $messages;
  }

  /**
   * Serialize the object.
   */
  public function serialize() {
    return drupal_json_encode($this->data);
  }

  /**
   * Unserialize the object.
   */
  public function unserialize($string) {
    $data = drupal_json_decode($string);
    if (!isset($data['time'])) {
      throw new CompostAgentException('Invalid message');
    }
    $this->data = $data;
  }
}


/**
 * The protocol service.
 */
class CompostAgentProtocolService {

  /**
   * Check if the current request method is valid for the protocol.
   */
  public function isValidMethod() {
    return $_SERVER['REQUEST_METHOD'] == 'POST';
  }

  /**
   * Create a request.
   *
   * @throws CompostAgentException
   *   If the message authenticity cannot be asserted.
   */
  public function newRequest($data, $agent_key, $manager_key) {
    $req = new CompostAgentRequest($_POST, $manager_key);
    if (!$req->isAuthentic()) {
      throw new CompostAgentException('Could not assert message authenticity');
    }

    return $req;
  }

  /**
   * Get a CompostAgentMessage from a request.
   */
  public function getRequestMessage(CompostAgentRequest $req, $agent_key, $manager_key) {
    // Get payload and (possibly) decipher it.
    $payload = $req->getPayload();
    $cipher_algo = $req->getCipherAlgo();
    if ($cipher_algo) {
      $cipher = _compostagent_get_cipher($cipher_algo);
      $payload = $cipher->decrypt(base64_decode($payload),
                                  $agent_key . $manager_key);
    }

    $msg = new CompostAgentMessage();
    $msg->unserialize($payload);

    // Now check message time, which we use as a nonce in our protocol
    // to prevent replay attacks. Basically, a message time must always
    // be greater than the last one received.
    //
    // Notice: message times are integers representing the number of
    // seconds since Unix epoch (a.k.a Unix timestamp). This means that
    // once a message is received, subsequent ones within the same
    // second will be rejected.
    $msg_time = $msg->getTime();
    if ($msg_time <= variable_get('compostagent_last_message', 0)) {
      throw new CompostAgentException('Message is out of order');
    }

    // Check and possibly reject messages that are "too old" or "too new".
    if (abs(REQUEST_TIME - $msg_time) > variable_get('compostagent_max_time_diff', 60)) {
      throw new CompostAgentException('Message too old/new. Are your clocks in sync?');
    }
    variable_set('compostagent_last_message', $msg_time);

    return $msg;
  }

  /**
   * Send a protocol reply.
   */
  public function reply($output, $agent_key, $manager_key, $cipher_algo) {

    if ($cipher_algo) {
      // We got a encrypted request, so we MUST issue an encrypted response.
      $encryption = 1;
    }
    else {
      $encryption = variable_get('compostagent_encryption', 0);

      // If encryption is not enforced, check if are using a secure
      // channel, and enable encryption if not.
      if (!$encryption) {
        $encryption = !_compostagent_is_secure();
      }
    }

    if ($encryption) {
      $cipher = _compostagent_get_cipher('aes-128-cbc');
      $output = $cipher->encrypt($output, $agent_key . $manager_key);
    }
    else {
      $cipher = NULL;
    }

    if ($cipher) {
      drupal_add_http_header('X-Compost-Cipher', $cipher->getName());
      drupal_add_http_header('Content-Type',
                             'application/octet-stream; charset=BINARY');
    }
    else {
      drupal_add_http_header('Content-Type', 'application/json; charset=UTF-8');
    }

    // Add MAC.
    drupal_add_http_header('X-Compost-MAC',
                           "hmac-sha256 " . base64_encode(hash_hmac('sha256',
                                                                    $output,
                                                                    $agent_key,
                                                                    TRUE)));

    drupal_add_http_header('Cache-control', 'no-cache, max-age=0, private');

    echo $output;
  }
}

/**
 * Collects all available commands.
 */
function _compostagent_request_commands() {
  $commands = array();

  foreach (module_implements('compost_command_info') as $module) {
    $function = $module . '_compost_command_info';
    if (function_exists($function)) {
      $result = call_user_func($function);
      if (isset($result)) {
        // No need to send callback name to the manager.
        unset($result['callback']);

        $commands[$module] = $result;
      }
    }
  }

  return $commands;
}


/**
 * Runs a command.
 *
 * @param array $args
 *   Command arguments. $args[0] is command name.
 */
function _compostagent_request_run($args) {
  if (!is_array($args)) {
    throw new CompostAgentException(t('Malformed arguments for run request'));
  }

  $parts = explode('.', $args[0], 3);
  if (count($parts) != 3) {
    throw new CompostAgentException(t('Invalid command: @command',
                                   array('@command' => $args[0])));
  }

  list($module, $prefix, $cmd) = $parts;

  $info = module_invoke($module, 'compost_command_info');
  if (!isset($info[$prefix]['commands'][$cmd])) {
    throw new CompostAgentException(t('No such command: @command',
                                   array('@command' => $args[0])));
  }

  $command = $info[$prefix]['commands'][$cmd];
  if (!isset($command['callback'])) {
    throw new CompostAgentException(t('No callback defined: @command',
                                   array('@command' => $args[0])));
  }
  elseif (!function_exists($command['callback'])) {
    throw new CompostAgentException(t('Invalid callback: @callback',
                                   array('@callback' => $command['callback'])));
  }

  $callback = $command['callback'];
  return array(
    'name' => $command['name'],
    'result' => $callback(),
  );
}


/**
 * Collects all alerts and handlers.
 */
function _compostagent_request_alerts() {
  $comps = array();

  foreach (module_implements('compost_alert_info') as $module) {
    $function = $module . '_compost_alert_info';
    if (function_exists($function)) {
      $result = call_user_func($function);
      if (isset($result)) {
        $comps[$module] = $result;
      }
    }
  }

  return $comps;
}


/**
 * Collects all registered variables.
 */
function _compostagent_request_variables() {
  $vars = array();

  foreach (module_implements('compost_variable_info') as $module) {
    $result = module_invoke($module, 'compost_variable_info');
    if ($result) {
      $vars[$module] = $result;
    }
  }

  return $vars;
}


/**
 * Process a Command Post message.
 */
function _compostagent_process_message(CompostAgentMessage $msg) {
  $resp = new CompostAgentMessage();

  foreach ($msg->getRequests() as $request => $args) {
    try {
      switch ($request) {
        case 'alerts':
          $result = _compostagent_request_alerts();
          break;

        case 'commands':
          $result = _compostagent_request_commands();
          break;

        case 'variables':
          $result = _compostagent_request_variables();
          break;

        case 'run':
          $result = _compostagent_request_run($args);
          break;

        default:
          watchdog('compostagent',
                   'Unsupported request %request',
                   array('%request' => $request),
                   WATCHDOG_NOTICE);
          break;
      }
    }
    catch (CompostAgentException $ex) {
      $result = NULL;
      // Add Drupal message so that we can return it after processing
      // all requests.
      drupal_set_message($ex->getMessage(), 'error');
    }

    $resp->addResponse($request, $result);
  }

  return $resp;
}


/**
 * Returns a cipher object.
 *
 * @param string $cipher_algo
 *   The cipher algorithm (e.g. "aes-128-cbc").
 *
 * @return CompostAgentCipher
 *   The cipher object.
 */
function _compostagent_get_cipher($cipher_algo) {
  static $ciphers = array();

  if (isset($ciphers[$cipher_algo])) {
    return $ciphers[$cipher_algo];
  }

  if ($cipher_algo != 'aes-128-cbc') {
    watchdog('compostagent',
             "Unsupported algorithm: %algorithm",
             array('%algorithm' => $cipher_algo),
             WATCHDOG_CRITICAL);
    header('HTTP/1.1 501 Not Implemented');
    exit();
  }

  if (function_exists('mcrypt_encrypt')) {
    $factory = new CompostAgentMCryptCipherFactory();
  }
  elseif (function_exists('openssl_encrypt')) {
    $factory = new CompostAgentOpenSSLCipherFactory();
  }
  else {
    watchdog('compostagent',
             "No mcrypt or openssl PHP extension found. Cannot encrypt/decrypt!",
             array(),
             WATCHDOG_CRITICAL);
    header('HTTP/1.1 501 Not Implemented');
    exit();
  }

  $cipher = $ciphers[$cipher_algo] = $factory->newCipher($cipher_algo);

  return $cipher;
}


/**
 * Page callback: handle the general protocol request.
 *
 * @see compostagent_menu()
 */
function _compostagent_cb_request() {

  $svc = new CompostAgentProtocolService();

  // Request must come via HTTP POST.
  if (!$svc->isValidMethod()) {
    watchdog('compostagent', 'Invalid request method %method',
             array('%method' => $_SERVER['REQUEST_METHOD']));
    header('HTTP/1.1 400 Bad Request');
    exit();
  }

  $agent_key = variable_get('compostagent_key', NULL);
  $manager_key = variable_get('compostagent_manager_key', NULL);

  if (!($agent_key && $manager_key)) {
    watchdog('compostagent',
             'Keys are not configured',
             array(),
             WATCHDOG_ERROR,
             l(t('configure'), "admin/config/system/compostagent"));

    header('HTTP/1.1 501 Not Implemented');
    exit();
  }

  try {
    $req = $svc->newRequest($_POST, $agent_key, $manager_key);
  }
  catch (CompostAgentException $ex) {
    watchdog('compostagent',
             'Protocol error: %message',
             array('%message' => $ex->getMessage()),
             WATCHDOG_ERROR);
    header('HTTP/1.1 400 Bad Request');
    exit();
  }

  // Request is OK -- get payload and (possibly) decipher it.
  $payload = $req->getPayload();
  $cipher_algo = $req->getCipherAlgo();
  if ($cipher_algo) {
    $cipher = _compostagent_get_cipher($cipher_algo);
    $payload = $cipher->decrypt(base64_decode($payload),
                                $agent_key . $manager_key);
  }

  try {
    $msg = $svc->getRequestMessage($req, $agent_key, $manager_key);
  }
  catch (CompostAgentException $ex) {
    watchdog('compostagent',
             $ex->getMessage(),
             array(),
             WATCHDOG_ERROR);
    header('HTTP/1.1 400 Bad Request');
    exit();
  }

  // Process the message.
  $resp = _compostagent_process_message($msg);

  // Load messages.
  $resp->setMessages(drupal_get_messages());

  $svc->reply($resp->serialize(), $agent_key, $manager_key, $cipher_algo);
}


/**
 * Page callback: handle the protocol request for pairing the site.
 *
 * @see compostagent_menu()
 */
function _compostagent_cb_request_pair() {
  $svc = new CompostAgentProtocolService();

  // Request must come via HTTP POST.
  if (!$svc->isValidMethod()) {
    watchdog('compostagent', 'Invalid request method %method',
             array('%method' => $_SERVER['REQUEST_METHOD']));
    header('HTTP/1.1 400 Bad Request');
    exit();
  }

  if (empty($_POST['salt'])) {
    watchdog('compostagent', 'Protocol error: missing salt');
    header('HTTP/1.1 400 Bad Request');
    exit();
  }

  $info = compostagent_get_pair_info();
  if (!$info) {
    watchdog('compostagent', 'Unexpected pairing request');
    header('HTTP/1.1 404 Not Found');
    exit();
  }

  $pin = $info['pin1'] . $info['pin2'];

  $count = strlen($info['pin1']) * 1000;
  $salt = base64_decode($_POST['salt']);

  $manager_pin_key = compostagent_pbkdf2('sha256', $pin,
                                         $salt, $count,
                                         16, TRUE);
  $agent_pin_key = compostagent_pbkdf2('sha256', $manager_pin_key,
                                       $salt, $count,
                                       16, TRUE);

  try {
    $req = $svc->newRequest($_POST, $agent_pin_key, $manager_pin_key);
  }
  catch (CompostAgentException $ex) {
    watchdog('compostagent',
             'Protocol error: %message',
             array('%message' => $ex->getMessage()),
             WATCHDOG_ERROR);
    header('HTTP/1.1 400 Bad Request');
    exit();
  }

  // Request is OK -- get payload and (possibly) decipher it.
  $payload = $req->getPayload();
  $cipher_algo = $req->getCipherAlgo();
  if ($cipher_algo) {
    $cipher = _compostagent_get_cipher($cipher_algo);
    $payload = $cipher->decrypt(base64_decode($payload),
                                $agent_pin_key . $manager_pin_key);
  }

  try {
    $msg = $svc->getRequestMessage($req, $agent_pin_key, $manager_pin_key);
  }
  catch (CompostAgentException $ex) {
    watchdog('compostagent',
             $ex->getMessage(),
             array(),
             WATCHDOG_ERROR);
    header('HTTP/1.1 400 Bad Request');
    exit();
  }

  $resp = new CompostAgentMessage();

  // Set a new keys.
  variable_set('compostagent_manager_key', $msg->getRequest('setkey'));
  variable_set('compostagent_key', drupal_random_key());
  variable_set('compostagent_last_message', 0);
  variable_del('compostagent_pair_info');

  $resp->addResponse('setkey', variable_get('compostagent_key'));

  $svc->reply($resp->serialize(), $agent_pin_key, $manager_pin_key, $cipher_algo);
}


/**
 * Page callback: display pairing information.
 *
 * @see compostagent_menu()
 */
function _compostagent_cb_pair() {

  $info = compostagent_get_pair_info();
  if (!$info) {
    $info = array(
      'pin1' => sprintf('%06d', mt_rand(0, 999999)),
      'pin2' => sprintf('%06d', mt_rand(0, 999999)),
      'timestamp' => REQUEST_TIME,
    );
    variable_set('compostagent_pair_info', $info);
    drupal_set_message(t('A new PIN was generated.'));
  }

  $timeout = compostagent_get_pair_timeout();

  drupal_add_http_header('Refresh',
                         ($timeout + 1) .
                         '; url=' . url('admin/config/system/compostagent'));

  return t('The pairing PIN is <code class="compostagent-pin">@pin</code>. It will expire in @interval.',
           array(
             '@pin' => $info['pin1'] . '-' . $info['pin2'],
             '@interval' => format_interval($info['timestamp'] - (REQUEST_TIME - $timeout), 1),
           ));
}

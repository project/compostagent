<?php
/**
 * @file
 * Administrative page callbacks for the Command Post agent module.
 */

require_once 'compostagent.inc';


/**
 * Form constructor for the agent settings form.
 *
 * @ingroup forms
 */
function _compostagent_settings_form() {
  $form = array();

  $form['compostagent_pair'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pair'),
    '#description' => t('If you are able to securely communicate a short-term PIN code to a Command Post administrator, you can pair your sites automatically. <em>Warning: pairing will replace all keys already configured</em>.'),
  );
  $form['compostagent_pair']['pair'] = array(
    '#type' => 'link',
    '#title' => t('Pair this site'),
    '#href' => 'compostagent/pair',
  );

  $form['compostagent_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Agent key'),
    '#default_value' => variable_get('compostagent_key', ''),
    '#description' => t('Enter this agent signature/encryption key. This key should be given to the Command Post administrator, so that this Drupal site can be monitored. <strong>You should keep this value secret.</strong>'),
    '#required' => TRUE,
  );

  $form['compostagent_manager_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Manager key'),
    '#default_value' => variable_get('compostagent_manager_key', ''),
    '#description' => t('Enter the Command Post key. The key should be given to you by the Command Post administrator. <strong>You should keep this value secret.</strong>'),
    '#required' => TRUE,
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $options = array(
    0 => t("Automatic (recommended)"),
    1 => t("Always encrypt"),
  );

  $form['advanced']['compostagent_encryption'] = array(
    '#type' => 'select',
    '#title' => t('Encryption'),
    '#default_value' => variable_get('compostagent_encryption', 0),
    '#options' => $options,
    '#description' => t('<em>Automatic</em> will make the agent omit encryption if the connection is secure (for example, when SSL is being used). <em>Always encrypt</em> will force the agent to always reply with encrypted responses, even under secure connections.'),
  );

  $mypath = drupal_get_path('module', 'compostagent');
  $form['#attached']['css'] = array("$mypath/compostagent.css");
  $form['#attached']['js'] = array("$mypath/compostagent.js");

  $form['#submit'][] = '_compostagent_settings_reset_last_message';

  return system_settings_form($form);
}


/**
 * A submit handler that resets the last received message timestamp.
 *
 * This is fired when the agent key changes in the settings form.
 */
function _compostagent_settings_reset_last_message($form, $form_state) {
  if ($form['compostagent_key']['#default_value'] != $form_state['values']['compostagent_key']) {
    variable_set('compostagent_last_message', REQUEST_TIME);
  }
}

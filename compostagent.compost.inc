<?php
/**
 * @file
 * Standard Command Post agent hooks.
 */


/**
 * Executes the compostagent.ping command.
 */
function _compostagent_cmd_compostagent_ping() {
  drupal_set_message(t("Hello from %site. I'm alive.",
                       array('%site' => variable_get('site_name'))));
}


/**
 * Executes the compostagent.time command.
 */
function _compostagent_cmd_compostagent_time() {
  drupal_set_message(t('Current time here is %time',
                       array('%time' => format_date(REQUEST_TIME, 'short'))));
}


/**
 * Executes the core.flush_caches command.
 */
function _compostagent_cmd_core_flush_caches() {
  drupal_flush_all_caches();
  drupal_set_message(t('Caches cleared.'));
}


/**
 * Executes the core.run_cron command.
 */
function _compostagent_cmd_core_run_cron() {
  if (drupal_cron_run()) {
    drupal_set_message(t('Cron ran successfully.'));
  }
  else {
    drupal_set_message(t('Cron run failed.'), 'error');
  }
}


/**
 * Implements hook_compost_command_info().
 */
function compostagent_compost_command_info() {
  $commands = array();

  $commands['core'] = array(
    'group' => t('Drupal core'),
    'commands' => array(
      'run_cron' => array(
        'name' => t('Run cron'),
        'description' => t('Run the Drupal cron'),
        'callback' => '_compostagent_cmd_core_run_cron',
      ),
      'flush_caches' => array(
        'name' => t('Flush caches'),
        'description' => t('Flush all Drupal caches'),
        'callback' => '_compostagent_cmd_core_flush_caches',
      ),
    ));

  $commands['compostagent'] = array(
    'group' => t('Command Post Agent'),
    'commands' => array(
      'ping' => array(
        'name' => t('Ping'),
        'description' => t('Ping the Command Post agent to see if it is still alive'),
        'callback' => '_compostagent_cmd_compostagent_ping',
      ),
      'time' => array(
        'name' => t('Get time'),
        'description' => t('Return the current time of the day of the Drupal server'),
        'callback' => '_compostagent_cmd_compostagent_time',
      ),
    ));

  return $commands;
}


/**
 * Get Drupal requirements array.
 */
function _compostagent_get_requirements() {
  static $requirements = NULL;

  if ($requirements === NULL) {
    include_once DRUPAL_ROOT . '/includes/install.inc';
    drupal_load_updates();
    $requirements = module_invoke_all('requirements', 'runtime');
  }

  return $requirements;
}


/**
 * Implements hook_compost_alert_info().
 */
function compostagent_compost_alert_info() {
  $alerts = array();

  $alerts['core'] = array(
    'group' => t('Drupal core'),
    'alerts' => array(),
  );

  foreach (_compostagent_get_requirements() as $name => $req) {
    if (isset($req['severity'])
        && ($req['severity'] == REQUIREMENT_WARNING
            || $req['severity'] == REQUIREMENT_ERROR)) {
      $fin_name = $name = preg_replace('/[^a-z0-9_]+/i', '_', $name);
      $c = 2;
      // Prevent name clashes after sanitizing by appending
      // a counter.
      while (isset($alerts['core']['alerts'][$fin_name])) {
        $fin_name = $name . '_' . $c;
        $c++;
      }
      $alerts['core']['alerts'][$fin_name] = array(
        'name' => $req['title'],
        'description' => $req['description'],
        'status' => ($req['severity'] == REQUIREMENT_WARNING ?
                     'notice' : 'fail'),
      );
    }
  }

  return $alerts;
}


/**
 * Gather server variables.
 */
function _compostagent_setup_server_vars(&$vars) {
  if (function_exists('apache_get_version')) {
    $software = 'Apache';
    $info = apache_get_version();

    if (preg_match('~^Apache/([\d+.]+)~', $info, $matches)) {
      $version = $matches[1];
    }
    else {
      $version = NULL;
    }
  }

  $vars['variables'] = array(
    'name' => array(
      'name' => t('HTTP server name'),
      'value' => $_SERVER['SERVER_NAME'],
    ),
    'host' => array(
      'name' => t('Server host name'),
      'value' => function_exists('gethostname') ? gethostname() : php_uname('n'),
    ),
    'software' => array(
      'name' => t('Server software'),
      'value' => $software,
    ),
    'info' => array(
      'name' => t('Software info'),
      'value' => $info,
    ),
    'version' => array(
      'name' => t('Software version'),
      'value' => array(
        'type' => 'v',
        'value' => $version,
      ),
    ),
  );
}


/**
 * Get mapped account registration value.
 */
function _compostagent_get_account_registration() {
  switch (intval(variable_get('user_register', -1))) {
    case 0:
      $reg = 'admin';
      break;

    case 1:
      $reg = 'public';
      break;

    case 2:
      $reg = 'moderated';
      break;

    default:
      $reg = 'unknown';
  }
  return $reg;
}


/**
 * Setup core variable values.
 */
function _compostagent_setup_core_vars(&$vars) {
  $vars['variables'] = array(
    'version' => array(
      'name' => t('Drupal version'),
      'value' => array(
        'type' => 'v',
        'value' => VERSION,
      ),
    ),
    'site_name' => array(
      'name' => t('Site name'),
      'value' => variable_get('site_name', ''),
    ),
    'logo' => array(
      'name' => t('Logo'),
      'value' => theme_get_setting('logo'),
    ),
    'site_mail' => array(
      'name' => t('E-mail address'),
      'value' => variable_get('site_mail', ''),
    ),
    'default_country' => array(
      'name' => t('Default country'),
      'value' => variable_get('site_default_country', ''),
    ),
    'file_default_scheme' => array(
      'name' => t('Default download method'),
      'value' => variable_get('file_default_scheme', ''),
    ),
    'install_profile' => array(
      'name' => t('Install profile'),
      'value' => variable_get('install_profile', ''),
    ),
    'account_registration' => array(
      'name' => t('Account registration'),
      'value' => _compostagent_get_account_registration(),
    ),
    'cron_last' => array(
      'name' => t('Last cron run'),
      'value' => array(
        'type' => 't',
        'value' => variable_get('cron_last', 0),
      ),
    ),
    'install_time' => array(
      'name' => t('Install time'),
      'value' => array(
        'type' => 't',
        'value' => variable_get('install_time', 0),
      ),
    ),
    'maintenance' => array(
      'name' => t('Maintenance status'),
      'value' => (bool) variable_get('maintentance_mode', FALSE),
    ),
  );
}


/**
 * Setup database variables.
 */
function _compostagent_setup_database_vars(&$vars) {
  foreach ($GLOBALS['databases'] as $db_name => $target) {
    foreach ($target as $target_name => $info) {
      $vars['variables'] = array(
        "$db_name.$target_name.driver" => array(
          'name' => t('Database !name/!target driver',
                      array(
                        '!name' => $db_name,
                        '!target' => $target_name,
                      )),
          'value' => $info['driver'],
        ),
        "$db_name.$target_name.host" => array(
          'name' => t('Database !name/!target host',
                      array(
                        '!name' => $db_name,
                        '!target' => $target_name,
                      )),
          'value' => $info['host'],
        ),
        "$db_name.$target_name.port" => array(
          'name' => t('Database !name/!target port',
                      array(
                        '!name' => $db_name,
                        '!target' => $target_name,
                      )),
          'value' => array(
            'type' => 'i',
            'value' => $info['port'] ? $info['port'] : NULL,
          ),
        ),
        "$db_name.$target_name.prefix" => array(
          'name' => t('Database !name/!target prefix',
                      array(
                        '!name' => $db_name,
                        '!target' => $target_name,
                      )),
          'value' => $info['prefix']),
      );
    }
  }
}


/**
 * Implements hook_compost_variable_info().
 */
function compostagent_compost_variable_info() {
  $vars = array();

  $vars['server'] = array(
    'group' => t('Server'),
  );

  _compostagent_setup_server_vars($vars['server']);

  $vars['core'] = array(
    'group' => t('Drupal core'),
  );

  _compostagent_setup_core_vars($vars['core']);

  $vars['database'] = array(
    'group' => t('Database'),
  );

  _compostagent_setup_database_vars($vars['database']);

  $modules_info = system_get_info('module');
  $themes_info = system_get_info('theme');

  $vars['php'] = array(
    'group' => t('PHP'),
    'variables' => array(
      'versioninfo' => array(
        'name' => t('PHP version info'),
        'value' => phpversion(),
      ),
      'version' => array(
        'name' => t('PHP version'),
        'value' => array(
          'type' => 'v',
          'value' => preg_replace('/[^\d.].*$/', '',
                                  phpversion()),
        ),
      ),
      'memory_limit' => array(
        'name' => t('PHP memory_limit setting'),
        'value' => _compostagent_strbytes(ini_get('memory_limit')),
      ),
    ),
  );

  $vars['modules'] = array(
    'group' => t('Modules'),
    'variables' => array(),
  );

  foreach ($modules_info as $name => $info) {
    $vars['modules']['variables']["$name.version"] = array(
      'name' => t('!module module version',
                  array('!module' => $info['name'])),
      'value' => array(
        'type' => 'v',
        'value' => isset($info['version']) ? $info['version'] : NULL,
      ));
    $vars['modules']['variables']["$name.enabled"] = array(
      'name' => t('!module is enabled',
                  array('!module' => $info['name'])),
      'value' => TRUE);
  }

  $vars['themes'] = array(
    'group' => t('Themes'),
    'variables' => array(),
  );

  foreach ($themes_info as $name => $info) {
    $vars['themes']['variables']["$name.version"] = array(
      'name' => t('!theme version',
                  array('!theme' => $info['name'])),
      'value' => array(
        'type' => 'v',
        'value' => isset($info['version']) ? $info['version'] : NULL,
      ),
    );
    $vars['themes']['variables']["$name.enabled"] = array(
      'name' => t('!theme is enabled',
                  array('!theme' => $info['name'])),
      'value' => TRUE,
    );
  }

  return $vars;
}
